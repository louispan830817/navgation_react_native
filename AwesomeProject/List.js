import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';

class List extends Component {
  state = {
    names: [
      {
        id: 0,
        name: 'GPM-FMD審核教學及測試',
      },
      {
        id: 1,
        name: 'IEC 80079-34防爆管理系統稽核',
      },
      {
        id: 2,
        name: 'IEC80079-34 防爆安全訓練',
      },
      {
        id: 3,
        name: '緯創綠色產品策略及有害物質管理介紹',
      },
      {
        id: 4,
        name: 'Power Bi Desktop',
      },
    ],
  };
  alertItemName = item => {
    alert(item.name);
  };
  render() {
    return (
      <View>
        {this.state.names.map((item, index) => (
          <TouchableOpacity
            key={item.id}
            style={styles.container}
            onPress={() => this.alertItemName(item)}>
            <Text style={styles.text}>{item.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}
export default List;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    marginTop: 0,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
  },
  text: {
    color: '#000000',
  },
});
