/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Button,
  ActivityIndicator,
  FlatList,
  Image,
  List,
  Dimensions
} from 'react-native';
import { NavigationContainer, StackActions, NavigationActions } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { ListItem } from 'react-native-elements';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { App } from './App';
import Animated from 'react-native-reanimated';
import Video from 'react-native-video';
//import Orientation from 'react-native-orientation-locker';
import { Login } from './Login';
import Swiper from 'react-native-swiper';
const { width } = Dimensions.get('window');
const listinformation = [
  {
    name: '通過條件(全部符合)',
    subtitle: '考試總分達到70分',
  },
  {
    name: '課程資訊',
    subtitle: '課程時長:0分鐘\n\n先修課程:N/A',
  },
  {
    name: '講師',
    subtitle: '報名人數:5\n\n費用: NT$ 0',
  },
];
const listclass = [
  {
    name: '1.MY_WMIH_QI_037零件風險分析作業規範',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
  },
  {
    name: '2.課程測驗',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
  },
];
const list = [
  {
    name: 'GPM-FMD審核教學及測試',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '2019 WIH',
  },
  {
    name: 'IEC 80079-34防爆管理系統稽核',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'IEC 80079-34防爆管理系統稽核',
  },
  {
    name: 'IEC80079-34 防爆安全訓練',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'IEC80079-34 防爆安全訓練',
  },
  {
    name: '緯創綠色產品策略及有害物質管理介紹',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '緯創綠色產品策略及有害物質管理介紹',
  },
  {
    name: 'Power Bi Desktop',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Power Bi Desktop',
  },
  {
    name: 'Power BI: 人人都可上手的報表製作分析工具(線上)',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '2019 WIH',
  },
  {
    name: 'WiNerve2 系統',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'WiNerve2操作影片',
  },
  {
    name: 'How To Secure Your Web Server',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '選修',
  },
];
function HomeScreen() {
  // Orientation.unlockAllOrientations();
  return (
    <View style={styles.container}>
      <Swiper
        height={200}
        loop={true}
        autoplay={true}
        autoplayTimeout={4}
        removeClippedSubviews={false}
        horizontal={true}
        paginationStyle={{ bottom: 10 }}
        showsButtons={false}
        dot={<View style={{           //未选中的圆点样式
          backgroundColor: 'rgba(0,0,0,.2)',
          width: 18,
          height: 18,
          borderRadius: 8,
          marginLeft: 10,
          marginRight: 9,
          marginTop: 9,
          marginBottom: 9,
        }} />}
        activeDot={<View style={{    //选中的圆点样式
          backgroundColor: '#717478',
          width: 18,
          height: 18,
          borderRadius: 8,
          marginLeft: 10,
          marginRight: 9,
          marginTop: 9,
          marginBottom: 9,
        }} />}>
        <Image source={require('./images/fox.jpg')} style={styles.img} />
        <Image source={require('./images/leopard.jpg')} style={styles.img} />
        <Image source={require('./images/lion.jpg')} style={styles.img} />
      </Swiper>
      {list.map((l, i) => (
        <ListItem
          style={styles.themeview}
          key={i}
          title={l.name}
          bottomDivider
        />
      ))}
    </View>
  );
}
function ClassScreen() {
  //Orientation.unlockAllOrientations();
  // <View>
  //   {list.map((l, i) => (
  //     <ListItem
  //       key={i}
  //       leftAvatar={{source: {uri: l.avatar_url}}}
  //       title={l.name}
  //       subtitle={l.subtitle}
  //       badge={{
  //         value: 999,
  //         textStyle: {color: 'black'},
  //         containerStyle: {marginTop: -20},
  //       }}
  //       bottomDivider
  //     />
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      const response = await fetch('https://data.coa.gov.tw/Service/OpenData/TransService.aspx?UnitId=QcbUEzN6E6DL')
      console.log("response", response);
      const responseJson = await response.json()
      console.log("responseJson", responseJson);
      setData(responseJson);
    }
    getData();
  })
  console.log('abc');
  // fetch(
  //   'https://data.coa.gov.tw/Service/OpenData/TransService.aspx?UnitId=QcbUEzN6E6DL'
  // )
  // .then(response => response.json())
  // .then(responseJson => setData(responseJson)) //可抓大項陣列
  // .catch(error => alert.error);
  // }, []);
  // try {
  //   const response = await fetch('https://data.coa.gov.tw/Service/OpenData/TransService.aspx?UnitId=QcbUEzN6E6DL');
  //   const json = await response.json()
  //   this.setState({ data: json });
  // } catch (error) {
  //   console.error(error);
  // }
  return (
    // <View style={{flex: 1, padding: 24}}>
    //   {isLoading ? (
    //     <ActivityIndicator />
    //   ) : (
    //     <FlatList
    //       data={data}
    //       keyExtractor={({id}, index) => id}
    //       renderItem={({item}) => (
    //         <Text>
    //           {item.login}, {item.avatar_url}
    //         </Text>
    //       )}
    //     />
    //   )}
    // </View>
    <View style={{ flex: 1, padding: 24 }}>
      <FlatList
        data={data}
        keyExtractor={({ item }, index) => item}
        renderItem={({ item }) => (
          <ListItem
            roundAvatar
            title={item.animal_place}
            subtitle={
              item.animal_foundplace +
              ' ' +
              item.animal_colour +
              '的' +
              item.animal_kind
            }
            leftAvatar={{
              source: {
                uri: item.album_file
                  ? item.album_file
                  : 'https://asms.coa.gov.tw/amlapp/upload/pic/47f43085-5d06-4106-bfc2-4b8bcbbd61ad_org.jpg',
              },
            }}
          />
        )}
      />
    </View>
  );
}
function ClassCheapScreen() {
  return (
    <View>
      {listclass.map((l, i) => (
        <ListItem
          style={styles.themeview}
          key={i}
          title={l.name}
          bottomDivider
        />
      ))}
      <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonText}> 1.ESD </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonText}> 2.課程考試 </Text>
      </TouchableOpacity>
    </View>
  );
}
function KnowledgeScreen() {
  //Orientation.lockToLandscape();
  return (
    <View style={{ flex: 1, flexDirection: 'row' }}>
      <Video
        source={require('./video/song.mp4')} //播放本機video source={require('../assets/video/turntable.mp4')}
        ref={ref => (this.player = ref)} // Store reference
        //onBuffer={this.onBuffer} // Callback when remote video is buffering
        //onError={this.videoError} // Callback when video cannot be loaded
        onBuffer={this.onBuffer}
        onProgress={this.onProgress}
        style={styles.backgroundVideo}
        controls={true}
        paused={false}
        fullscreen={true}
        resizeMode="contain"
      />
    </View>
  );
}
function LearnScreen() {
  return (
    <View>
      {listinformation.map((l, i) => (
        <ListItem key={i} title={l.title} subtitle={l.subtitle} bottomDivider />
      ))}
    </View>
  );
}

function MyTabBar({ state, descriptors, navigation, position }) {
  //Orientation.unlockAllOrientations();
  return (
    <View style={{ flexDirection: 'row', paddingTop: 20 }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };
        // modify inputRange for custom behavior
        const inputRange = state.routes.map((_, i) => i);
        const opacity = Animated.interpolate(position, {
          inputRange,
          outputRange: inputRange.map(i => (i === index ? 1 : 0)),
        });

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1 }}>
            <Animated.Text style={{ opacity }}>{label}</Animated.Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
const Tab = createMaterialBottomTabNavigator();
const TabTop = createMaterialTopTabNavigator();
const StackClass = createStackNavigator();
const StackHome = createStackNavigator();
const StackKnowledge = createStackNavigator();
const StackLearn = createStackNavigator();
const Drawer = createDrawerNavigator();
const tabnavigator = value => (
  <Tab.Navigator
    initialRouteName={{ value }}
    activeColor="#6586c9"
    inactiveColor="#a3a3a3"
    barStyle={{ backgroundColor: '#FFFF' }}>
    <Tab.Screen name="首頁">
      {() => (
        <StackHome.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
          <StackHome.Screen name="首頁" component={HomeScreen} />
        </StackHome.Navigator>
      )}
    </Tab.Screen>
    <Tab.Screen name="課程庫">
      {() => (
        <StackClass.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
          <StackClass.Screen name="課程庫" component={ClassScreen} />
        </StackClass.Navigator>
      )}
    </Tab.Screen>
    <Tab.Screen name="知識庫">
      {() => (
        <StackKnowledge.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
          <StackKnowledge.Screen name="知識庫" component={KnowledgeScreen} />
        </StackKnowledge.Navigator>
      )}
    </Tab.Screen>
    <Tab.Screen name="我的學習">
      {() => (
        <StackLearn.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
          <StackLearn.Screen name="我的學習">
            {() => (
              <TabTop.Navigator tabBar={props => <MyTabBar {...props} />}>
                <TabTop.Screen name="課程資訊" component={LearnScreen} />
                <TabTop.Screen name="課程章節" component={ClassCheapScreen} />
              </TabTop.Navigator>
            )}
          </StackLearn.Screen>
        </StackLearn.Navigator>
      )}
    </Tab.Screen>
  </Tab.Navigator>
);
const useremark = () => (
  <View style={styles.container}>
    <Swiper
      height={200}
      loop={true}
      autoplay={true}
      autoplayTimeout={4}
      removeClippedSubviews={false}
      horizontal={true}
      paginationStyle={{ bottom: 10 }}
      showsButtons={false}
      dot={<View style={{           //未选中的圆点样式
        backgroundColor: 'rgba(0,0,0,.2)',
        width: 18,
        height: 18,
        borderRadius: 8,
        marginLeft: 10,
        marginRight: 9,
        marginTop: 9,
        marginBottom: 9,
      }} />}
      activeDot={<View style={{    //选中的圆点样式
        backgroundColor: '#717478',
        width: 18,
        height: 18,
        borderRadius: 8,
        marginLeft: 10,
        marginRight: 9,
        marginTop: 9,
        marginBottom: 9,
      }} />}>
      <Image source={require('./images/fox.jpg')} style={styles.img} />
      <Image source={require('./images/leopard.jpg')} style={styles.img} />
      <Image source={require('./images/lion.jpg')} style={styles.img} />
    </Swiper>
  </View>
)
// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [navigation.navigate({ routeName: 'Login' })],
// });

export default function Learn(props) {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="首頁">
        <Drawer.Screen name="首頁">{() => tabnavigator('首頁')}</Drawer.Screen>
        <Drawer.Screen name="課程庫">
          {() => tabnavigator('課程庫')}
        </Drawer.Screen>
        <Drawer.Screen name="知識庫">
          {() => tabnavigator('知識庫')}
        </Drawer.Screen>
        <Drawer.Screen name="我的學習">
          {() => tabnavigator('我的學習')}
        </Drawer.Screen>
        <Drawer.Screen name="使用說明">
          {() => useremark()}
        </Drawer.Screen>
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  fullScreen: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  themeview: {
    marginTop: 5,
    borderRadius: 25,
    elevation: 1.5,
    shadowColor: '#E0E0E0',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 1,
    shadowRadius: 1.5,
    marginBottom: 3,
  },
  button: {
    margin: 12,
    padding: 30,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: '#FFFFFF',
    borderRadius: 9,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#3C3C3C',
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 2, height: 18 },
  },
  buttonText: {
    marginRight: 'auto',
    color: '#3C3C3C',
    fontSize: 20,
    fontWeight: 'bold',
  },
  swiper: {},
  img: {
    width: width,
    flex: 1,
  },
  wrapper: {
  }
});
