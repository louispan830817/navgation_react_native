/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, Component } from 'react';
import { Text, View, StyleSheet, TextInput, Button, AppRegistry } from 'react-native';
import { NavigationContainer, NavigationActions } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { ListItem } from 'react-native-elements';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Value } from 'react-native-reanimated';
import Learn from './Learn.js';
export default function Login(props) {

    const [password, setpasword] = useState('');
    const [result, setresult] = useState('');
    const resultTrue = '輸入正確';
    const resultFalse = '請重新輸入手機號碼';
    const confirm = () => {
        if (password === '0912345678') {
            setresult(resultTrue)
            props.navigation.navigate(Learn);
        } else {
            setresult(resultFalse)
        }
    }
    return (
        <View style={styles.container}>
            <Text style={styles.text}>電話號碼:0912345678</Text>
            <TextInput
                style={{ height: 50, width: 300, borderRadius: 2, borderColor: 'gray', borderWidth: 5, backgroundColor: 'gray', color: 'white', fontSize: 28, textAlign: 'center' }}
                onChangeText={(text) => setpasword(text)}
                value={password}
                maxLength={10}
                placeholder='set your phonenumber'
                keyboardType={"numeric"}
                secureTextEntry={true}//密碼形式
                editable={true}//設置權限
                autoFocus={true}
            />
            <Text style={styles.text}>您輸入的電話號碼是{password}</Text>
            <Button
                title="confirm"
                onPress={() => confirm()}
            />
            <Text>{setresult}</Text>
            <Text>{result}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 25
    }
});
