/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import {Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {ListItem} from 'react-native-elements';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {Value} from 'react-native-reanimated';
import {Learn} from './Learn.js';
const list = [
  {
    name: 'GPM-FMD審核教學及測試',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '2019 WIH',
  },
  {
    name: 'IEC 80079-34防爆管理系統稽核',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'IEC 80079-34防爆管理系統稽核',
  },
  {
    name: 'IEC80079-34 防爆安全訓練',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'IEC80079-34 防爆安全訓練',
  },
  {
    name: '緯創綠色產品策略及有害物質管理介紹',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '緯創綠色產品策略及有害物質管理介紹',
  },
  {
    name: 'Power Bi Desktop',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Power Bi Desktop',
  },
  {
    name: 'Power BI: 人人都可上手的報表製作分析工具(線上)',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '2019 WIH',
  },
  {
    name: 'WiNerve2 系統',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'WiNerve2操作影片',
  },
  {
    name: 'How To Secure Your Web Server',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '選修',
  },
];

function HomeScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>首頁!</Text>
    </View>
  );
}

function ClassScreen() {
  return (
    <View>
      {list.map((l, i) => (
        <ListItem
          key={i}
          leftAvatar={{source: {uri: l.avatar_url}}}
          title={l.name}
          subtitle={l.subtitle}
          badge={{
            value: 999,
            textStyle: {color: 'black'},
            containerStyle: {marginTop: -20},
          }}
          bottomDivider
        />
      ))}
    </View>
  );
}
function KnowledgeScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>知識庫!!!</Text>
    </View>
  );
}
function LearnScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>我的學習!</Text>
    </View>
  );
}
// toggleDrawer = () => {
//   //Props to open/close the drawer
//   this.props.navigationProps.toggleDrawer();
//     };
// render() {
//   return (
//     <View style={{ flexDirection: 'row' }}>
//       <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
//         <Image
//           source={require('../images/drawer.png')}
//           style={{ width: 25, height: 25, marginLeft: 20, tintColor: '#ffffff' }}
//        />
//       </TouchableOpacity>
//     </View>
//   );
// }
const Tab = createMaterialBottomTabNavigator();
const StackClass = createStackNavigator();
const StackHome = createStackNavigator();
const StackKnowledge = createStackNavigator();
const StackLearn = createStackNavigator();
const Drawer = createDrawerNavigator();
const tabnavigator = value => (
  <Tab.Navigator
    initialRouteName={{value}}
    activeColor="#6586c9"
    inactiveColor="#a3a3a3"
    barStyle={{backgroundColor: '#FFFF'}}>
    <Tab.Screen name="首頁">
      {() => (
        <StackHome.Navigator screenOptions={{headerTitleAlign: 'center'}}>
          <StackHome.Screen name="首頁" component={HomeScreen} />
        </StackHome.Navigator>
      )}
    </Tab.Screen>
    <Tab.Screen name="課程庫">
      {() => (
        <StackClass.Navigator screenOptions={{headerTitleAlign: 'center'}}>
          <StackClass.Screen name="課程庫" component={ClassScreen} />
        </StackClass.Navigator>
      )}
    </Tab.Screen>
    <Tab.Screen name="知識庫">
      {() => (
        <StackKnowledge.Navigator screenOptions={{headerTitleAlign: 'center'}}>
          <StackKnowledge.Screen name="知識庫" component={KnowledgeScreen} />
        </StackKnowledge.Navigator>
      )}
    </Tab.Screen>
    <Tab.Screen name="我的學習">
      {() => (
        <StackLearn.Navigator screenOptions={{headerTitleAlign: 'center'}}>
          <StackLearn.Screen name="我的學習" component={Learn} />
        </StackLearn.Navigator>
      )}
    </Tab.Screen>
  </Tab.Navigator>
);
export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="首頁">
        <Drawer.Screen name="首頁">{() => tabnavigator('首頁')}</Drawer.Screen>
        <Drawer.Screen name="課程庫">
          {() => tabnavigator('課程庫')}
        </Drawer.Screen>
        <Drawer.Screen name="知識庫">
          {() => tabnavigator('知識庫')}
        </Drawer.Screen>
        <Drawer.Screen name="我的學習">
          {() => tabnavigator('我的學習')}
        </Drawer.Screen>
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
